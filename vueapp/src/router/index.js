import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import FirstComponent from '@/components/FirstComponent'
import FirstComponentChild from '@/components/FirstComponentChild'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/first/:name',
      name: 'FirstComponent',
      component: FirstComponent,
      children: [
        {
          path: 'child',
          component: FirstComponentChild
        }
      ]
    }
  ]
})
